// ==UserScript==
// @name        Voat Chat Enhancement Suite
// @namespace   memmove.net
// @description Adds various improvements for the Voat Chat.
// @include     https://voat.co/chat/
// @include     https://voat.co/chat/*
// @version     Alpha 5
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_deleteValue
// @grant       unsafeWindow
// ==/UserScript==

var version = "Alpha 5";

// jConstruct
jConstruct=function(){var t=function(){};return t.prototype.construct=function(t,o,e){if(!t)throw"Wrong number of arguments";o||(o={}),e||(e=[]);var r=document.createElement(t);!function(t){for(var o in t)t.hasOwnProperty(o)&&r.setAttribute(o,t[o])}(o),"[object Array]"!==Object.prototype.toString.call(e)&&(e=[e]);for(var n=0;n<e.length;n++)r.appendChild("object"==typeof e[n]?e[n]:this.text(""+e[n]));return r},t.prototype.text=function(t){return document.createTextNode(t)},t.prototype.c=t.prototype.construct,t.prototype.t=t.prototype.text,new t}();

var optionList = ["unicodeChat","stylepatch","linkpatch","timestamps","timestamps_iso",
                  "usertagging","unreadCounter","antiHeading","antiBlockquote","antiLists"];
var allOptions = {"unicodeChat":[true,0,"Enable Unicode patch","Automatically encodes Unicode characters as HTML entities"],
                  "stylepatch":[true,0,"Enable dubbelnougat UI","A custom UI for the chat designed by @dubbelnougat"],
                  "linkpatch":[true,0,"Open links in a new tab",""],
                  "timestamps":[true,0,"Display timestamps","Display timestamps in chat messages"],
                  "timestamps_iso":[false,1,"Timestamps in ISO format","If disabled, timestamps are formatted according to locale"],
                  "usertagging":[true,0,"User tag button","Display a button left from user links for easy tagging"],
                  "unreadCounter":[true,0,"Unread counter","Show counter of unread messages in the title of the tab"],
                  "antiHeading":[true,0,"Disable headings","They cause weird layout problems and are a nuisance"],
                  "antiBlockquote":[true,0,"Disable block quotes","They cause weird layout problems"],
                  "antiLists":[true,0,"Disable lists","They cause weird layout problems"]};
function enabled(x) {
    return GM_getValue("VCES_" + x, allOptions[x][0]);
}
function setEnabled(x, y) {
    GM_setValue("VCES_" + x, y);
}
var settingsWindow = document.createElement("div");
settingsWindow.setAttribute("style","position:absolute;top:0;left:0;right:0;bottom:0;opacity:0.9;z-index:100;padding:16px");
settingsWindow.style.backgroundColor=window.getComputedStyle(document.body).backgroundColor;
settingsWindow.id="vcesSettings";
document.body.appendChild(settingsWindow);
function hideSettings() {
    document.getElementById("vcesSettings").style.display="none";
    document.body.style.overflow="hidden";
}
function showSettings() {
    document.getElementById("vcesSettings").style.display="block";
    document.body.style.overflow="scroll";
}

var topbar = document.querySelectorAll("span.button")[0].parentNode;
var settingsButton = jConstruct.c("span", {"class": "button", "style": "float:right"}, []);
var settingsButtonA = jConstruct.c("a", {"class": "btn-whoaverse", "href": "#", "title": "Voat Chat Enhancement Suite, Version: " + version}, [jConstruct.t("VCES")]);
settingsButtonA.addEventListener("click", showSettings);
settingsButton.appendChild(settingsButtonA);
topbar.appendChild(settingsButton);
var settingsButtonA2 = jConstruct.c("a", {"class": "btn-whoaverse", "href": "#"}, [jConstruct.t("Close")]);
var settingsButton2 = settingsButton.cloneNode();
settingsButtonA2.addEventListener("click", hideSettings);
settingsButton2.appendChild(settingsButtonA2);
settingsWindow.appendChild(settingsButton2);
var reloadwarning = jConstruct.c("div", {"style": "font-weight:bold;font-size:large", "id": "reload-warning"}, [jConstruct.t("Please reload the page to save changes!")]);
reloadwarning.style.display = "none";
settingsWindow.appendChild(reloadwarning);
var optionsElem = document.createElement("ul");
for (var _i = 0; _i < optionList.length; _i++) {
    var optionname = optionList[_i];
    var option = allOptions[optionname];
    var checkbox = jConstruct.c("input",enabled(optionname) ? {"type":"checkbox","vces-option":optionname,"checked":""} : {"type":"checkbox","vces-option":optionname},[]);
    var decent = jConstruct.c("li", {"style": "margin-left:" + ((option[1] * 5) + []) + "em"}, [jConstruct.c("label", {}, [
        checkbox,
        jConstruct.t(" " + option[2]),
    ]), jConstruct.c("p", {}, [jConstruct.t(option[3])]), jConstruct.c((_i < (optionList.length - 1) && allOptions[optionList[_i+1]][1]>0) ? "span" : "br", {}, [])]);
    checkbox.addEventListener("change", function(e){setEnabled(e.target.getAttribute("vces-option"),e.target.checked);document.getElementById("reload-warning").style.display="block"});
    optionsElem.appendChild(decent);
}
settingsWindow.appendChild(optionsElem);
hideSettings();

if (enabled("unicodeChat")) {
  UTF32_fromCharCode = function(i) {
      if (typeof i === "object") {
          var s = "";
          for (var j = 0; j < i.length; j++) {
              s += UTF32_fromCharCode(i[j]);
          }
          return s;
      }
      if (i >= 0xD800 && i <= 0xDFFF)
          return "";
      if (i < 0)
          return "\uFFFD";
      if (i > 0x10FFFF)
          return "";
      if (i <= 0xFFFF)
          return String.fromCharCode(i);
      return String.fromCharCode(0xd800 + (((i - 0x10000) >> 10) & 0x3ff)) + String.fromCharCode(0xdc00 + ((i - 0x10000) & 0x3ff));
  }
  UTF32_toCharCodes = function(s) {
      var cc = [];
      var sur = false;
      var tt = 0;
      for (var i = 0; i < s.length; i++) {
          var x = s.charCodeAt(i);
          if ((x >= 0xD800 && x < 0xDC00) && !sur) {
              sur = true;
              tt = x & 0x3ff;
          } else if ((x >= 0xDC00 && x < 0xE000) && sur) {
              sur = false;
              cc.push(0x10000 + ((tt << 10) | (x & 0x3ff)));
          } else {
              cc.push(x);
          }
      }
      return cc;
  }
  UTF32String = function(str) {
      var ds = UTF32_toCharCodes(str);
      this.data = ds;
      this.length = ds.length;
  }
  UTF32String.buildArray = function(a) {
      var b = new UTF32String("");
      b.data = a;
      b.length = a.length;
      return b;
  }
  UTF32String.prototype.charAt = function(pos) {
      return UTF32_fromCharCode(this.data[pos]);
  }
  UTF32String.prototype.charCodeAt = function(pos) {
      return this.data[pos];
  }
  UTF32String.prototype.codePointAt = function(pos) {
      return this.data[pos];
  }
  UTF32String.prototype.concat = function(a) {
      return UTF32String.buildArray(this.data.concat(a.data));
  }
  UTF32String.prototype.indexOf = function(s) {
      var n = UTF32_toCharCodes(s);
      for (var i = 0; i <= s.length - n.length; i++) {
          var match = true;
          for (var j = 0; j < n.length; j++) {
              if (n[j] != s[i+j]) {
                  match = false;
                  break;
              }
          }
          if (!match) continue;
          return i;
      }
      return -1;
  }
  UTF32String.prototype.lastIndexOf = function(s) {
      var n = UTF32_toCharCodes(s);
      for (var i = s.length - n.length; i >= 0; i++) {
          var match = true;
          for (var j = 0; j < n.length; j++) {
              if (n[j] != s[i+j]) {
                  match = false;
                  break;
              }
          }
          if (!match) continue;
          return i;
      }
      return -1;
  }
  UTF32String.prototype.slice = function(a, b) {
      var text = [];
      if (typeof b === "undefined")
          b = this.data.length;
      if (a < 0)
          a += this.data.length;
      if (b < 0)
          b += this.data.length;
      for (var i = a; i < Math.min(this.data.length, b); i++)
          text.push(this.data[i]);
      return UTF32String.buildArray(text);
  }
  UTF32String.prototype.substr = function(a, b) {
      var text = [];
      if (a < 0)
          a += this.data.length;
      for (var i = a; i < Math.min(this.data.length, a + b); i++)
          text.push(this.data[i]);
      return UTF32String.buildArray(text);
  }
  UTF32String.prototype.substring = function(a, b) {
      var text = [];
      if (a < 0)
          a += this.data.length;
      if (b < 0)
          b += this.data.length;
      for (var i = a; i < Math.min(this.data.length, b); i++)
          text.push(this.data[i]);
      return UTF32String.buildArray(text);
  }
  UTF32String.prototype.toString = function() {
      return UTF32_fromCharCode(this.data);
  }
  UTF32String.prototype.valueOf = function() {
      return UTF32_fromCharCode(this.data);
  }
  var interval = setInterval( function () {try { sendChatMessage_orig = unsafeWindow.jQuery.connection.messagingHub.server.sendChatMessage;
  MAXCHARS = 200;
  htmlEntities=function(x){x=new UTF32String(x);a=[];for(var i=0;i<x.length;i++)a.push(x.charCodeAt(i));var b = a.map(function(x){return (x>=256||x<32)?"&#"+(x+[])+";":String.fromCharCode(x)});r="";for(var i=0;i<b.length;i++){if((r.length+b[i].length)>MAXCHARS)break;r+=b[i]};return r};
  var funcobj=function(a,n){sendChatMessage_orig(a,htmlEntities(n))};unsafeWindow.jQuery.connection.messagingHub.server.sendChatMessage=exportFunction(funcobj,unsafeWindow);clearInterval(interval) } catch (e) {console.error(e)}},500);
}

if (enabled("stylepatch")) {
  document.body.appendChild((function(){var x=document.createElement("style");x.innerHTML=`.subverseChatRoomContent {
    height: calc(100vh - 240px);
    max-height: calc(100vh - 240px);
  }
  .subverseChatRoomContent p {
    font-size: 11pt; 
    display: inline;
  }
  .subverseChatRoomContent p b a::before {
    display: block;
    content: " ";
  }
  .subverseChatRoomContent p:nth-child(even)::after {
    content: " ";
  }
  .footer-container {
    position: absolute;
    left: 2%;
    right: 2%;
    bottom: 0;
  }
  body {
    overflow: hidden; 
  }
  #chatInputBox {
    display: inline-block; 
    width: calc(100% - 204px);
  }
  .btn-whoaverse.contribute {
    width: 200px;
  }`;return x})());
  
}

var tabUnfocused = false;
var unfocusedMsg = 0;

function antiQuoteWrap(el,t) {
    if (el.length < 1) return;
    var b = [];
        for (var _i = 0; _i < el.length; _i++) {
          var node = el[_i];
          try {
          if (node.nodeName.toLowerCase()=="blockquote") {
              b = Array.concat(b, node.children);
              for (var _j = 0; _j < node.children.length; _j++) {
                  node.parentNode.insertBefore(node.children[_j], node);
              }
              node.parentNode.removeChild(node);
          } else if (node.nodeName.toLowerCase()=="p") {
              node.innerHTML = t + node.innerHTML;
          };
          } catch (e) {}
        };
    antiQuoteWrap(b,t+">");
}
var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if (enabled("linkpatch")) {
             try {
                for (var _i = 0; _i < mutation.addedNodes.length; _i++) {
                    var node = mutation.addedNodes[_i];
                    if (node.nodeName.toLowerCase()=="a") {
                        node.setAttribute("target", "_blank");
                    };
                    for (var _j = 0; _j < node.childNodes.length; _j++) {
                        var cnode = node.childNodes[_j];
                        if (cnode.nodeName.toLowerCase()=="a") {
                            cnode.setAttribute("target", "_blank");
                        } else if (cnode.nodeName.toLowerCase()=="b") {
                            for (var _k = 0; _k < cnode.childNodes.length; _k++) {
                                var ccnode = cnode.childNodes[_k];
                                if (ccnode.nodeName.toLowerCase()=="a") {
                                    ccnode.setAttribute("target", "_blank");
                                };
                            };
                        };
                    };
                };
             } catch (e) {console.error(e)}
            }
            if (enabled("timestamps")) {
             try {
                var mod2 = 0;
                for (var _i = 0; _i < mutation.addedNodes.length; _i++) {
                  var node = mutation.addedNodes[_i];
                  if (node.nodeName.toLowerCase()=="p") {
                    if ((mod2++)%2 == 1) { // this is to only add it to the second p element, which contains the message
                      var ts = document.createElement("span");
                      if (enabled("timestamps_iso"))
                        ts.innerHTML = "   (" + new Date().toISOString() + ")";  
                      else
                        ts.innerHTML = "   (" + new Date().toLocaleString() + ")";
                      ts.setAttribute("style", "font-size: x-small; float: right; color: #888");
                      var empty = document.createElement("span");
                      node.parentNode.insertBefore(ts, node.nextSibling);
                      node.parentNode.insertBefore(empty, node.nextSibling);
                    }
                  };
                };
             } catch (e) {console.error(e)}
            }
            if (enabled("usertagging")) {
             try {
                var mod2 = 0;
                for (var _i = 0; _i < mutation.addedNodes.length; _i++) {
                  var node = mutation.addedNodes[_i];
                  if (node.nodeName.toLowerCase()=="p") {
                    if ((mod2++)%2 == 0) { // users only
                      if (node.children[0].nodeName.toLowerCase()=="b") {
                        if (node.children[0].children[0].nodeName.toLowerCase()=="a") {
                            var ts = document.createElement("span");
                            ts.innerHTML = "[@]";  
                            ts.setAttribute("style", "cursor: pointer; font-size: x-small; float: left; margin-right: 0.5em; color: #888");
                            var cnode = node.children[0].children[0];
                            var user = "@" + cnode.innerHTML;
                            ts.addEventListener("click", function(){
                               document.getElementById("chatInputBox").value += user + " "; 
                               document.getElementById("chatInputBox").focus();
                            });
                            cnode.parentNode.insertBefore(ts, cnode.nextSibling);
                        }
                      }
                    }
                  };
                };   
             } catch (e) {console.error(e)}
            } 
            if (enabled("antiLists")) {
             try {
                for (var _i = 0; _i < mutation.addedNodes.length; _i++) {
                  var node = mutation.addedNodes[_i];
                  if (node.nodeName.toLowerCase()=="ul") {
                    var pnode = document.createElement("p");
                    pnode.innerHTML = "* " + node.querySelectorAll("li")[0].innerHTML;
                    node.parentNode.insertBefore(pnode, node);
                    node.parentNode.removeChild(node);
                  } else if (node.nodeName.toLowerCase()=="ol") {
                    var pnode = document.createElement("p");
                    pnode.innerHTML = "1. " + node.querySelectorAll("li")[0].innerHTML;
                    node.parentNode.insertBefore(pnode, node);
                    node.parentNode.removeChild(node);
                  };
                };   
             } catch (e) {console.error(e)}
            } 
            if (enabled("antiBlockquote")) {
                try {
                  antiQuoteWrap(mutation.addedNodes,"");
             } catch (e) {console.error(e)}
            };
            if (enabled("antiHeading")) {
                try {
                    for (var _i = 0; _i < mutation.addedNodes.length; _i++) {
                      var node = mutation.addedNodes[_i];
                      if (node.nodeName.toLowerCase()[0]=="h" && +node.nodeName[1] == +node.nodeName[1]) {
                            var pnode = document.createElement("b");
                            pnode.innerHTML = node.innerHTML;
                            node.parentNode.replaceChild(pnode, node);
                      };
                      for (var _j = 0; _j < node.childNodes.length; _j++) {
                        var cnode = node.childNodes[_j];
                        if (cnode.nodeName.toLowerCase()[0]=="h" && +cnode.nodeName[1] == +cnode.nodeName[1]) {
                          var pnode = document.createElement("b");
                          pnode.innerHTML = cnode.innerHTML;
                          cnode.parentNode.replaceChild(pnode, cnode);
                        };
                      };
                    }; 
             } catch (e) {console.error(e)}
            }
            if (enabled("unreadCounter") && tabUnfocused) {
                try {
                   var mod2 = 0;
                    for (var _i = 0; _i < mutation.addedNodes.length; _i++) {
                      var node = mutation.addedNodes[_i];
                      if (node.nodeName.toLowerCase()=="p") {
                        if ((mod2++)%2 == 0) { // users only
                          unfocusedMsg++;
                          document.title="(" + (unfocusedMsg + []) + ") " + document.querySelectorAll("h1.alert-h1")[0].innerHTML;
                        }
                      };
                    };
             } catch (e) {console.error(e)}
            }
        });    
});
var config = { attributes: true, childList: true, characterData: true, subtree: true };
observer.observe(document.body, config);

if (enabled("unreadCounter")) {
  document.title=document.querySelectorAll("h1.alert-h1")[0].innerHTML;
  window.addEventListener("focus",function(){tabUnfocused = false;unfocusedMsg = 0;document.title=document.querySelectorAll("h1.alert-h1")[0].innerHTML});
  window.addEventListener("blur",function(){tabUnfocused = true});
}